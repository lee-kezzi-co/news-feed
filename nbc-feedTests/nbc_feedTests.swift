//
//  nbc_feedTests.swift
//  nbc-feedTests
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import XCTest
@testable import nbc_feed

class nbc_feedTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoadFeed() {
        let expectation = XCTestExpectation(description: "Download NBC feed")

        let nbcUrl = URL(string: "https://s3.amazonaws.com/shrekendpoint/news.json")!
        JSONClient.request(nbcUrl) { json, error in
            
            guard let json = json else  {
                XCTFail("json missing from response")
                return
            }
            
            let feed = NBCFeed(json: json)
            
            XCTAssert(feed.items.count > 0)
            
            
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }


}
