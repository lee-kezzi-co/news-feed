//
//  HeroTableViewCell.swift
//  nbc-feed
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class HeroTableViewCell: UITableViewCell {
    var content: NBCContentHero!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
