//
//  ArticleTableViewCell.swift
//  nbc-feed
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var teaserImageView: UIImageView!
    @IBOutlet weak var publishedLabel: UILabel!
    
    @IBOutlet weak var summaryLabel: UILabel!
    
    var content: NBCContentArticle! {
        didSet {
            self.headlineLabel.text = content.headline
            self.teaserImageView.sd_setImage(with: content.teaser)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
