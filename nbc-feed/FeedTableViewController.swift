//
//  FeedTableViewController.swift
//  nbc-feed
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {
    var feed: NBCFeed! {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        guard feed != nil else { return 0 }

        return feed.items.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let item = feed.items[section] as? NBCContentSection {
            return item.items.count
        }
        
        if let _ = feed.items[section] as? NBCContentHero {
            return 2
        }
        
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let section = feed.items[indexPath.section] as? NBCContentSection {
            return cell( section.items[indexPath.row] )
        }
        
        if let hero = feed.items[indexPath.section] as? NBCContentHero {
            if indexPath.row == 0 {
                return cell( hero )
            }
            
            if indexPath.row == 1 {
                return cell( hero.item )
            }
        }
        
        return cell( feed.items[indexPath.section] )
    }

    private func cell(_ content: NBCContent) -> UITableViewCell {
        
        if let content = content as? NBCContentHero {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "hero-cell") as! HeroTableViewCell
            cell.content = content
            return cell
        }
        
        if let content = content as? NBCContentWeb {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "web-cell") as! WebTableViewCell
            cell.content = content
            return cell
        }
        
        if let content = content as? NBCContentVideo {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "video-cell") as! VideoTableViewCell
            cell.content = content
            return cell
        }
        
        if let content = content as? NBCContentArticle {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "article-cell") as! ArticleTableViewCell
            cell.content = content
            return cell
        }
        
        return self.tableView.dequeueReusableCell(withIdentifier: "empty-cell")!

    }
}
