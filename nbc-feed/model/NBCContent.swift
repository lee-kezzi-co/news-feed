//
//  NBCContent.swift
//  super-video
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import SwiftyJSON

class NBCContent: NSObject {
    
    internal let store: JSON
    
    var id: String {
        get { return store["id"].stringValue }
    }
    
    var url: URL? {
        get { return store["url"].url }
    }
    
    func value(key: String) -> Any? {
        return store["key"].object
    }
    
    
    required init?(json: JSON) {
        self.store = json
    }
    
    var tracking: NSObject?
    
    class func type(json: JSON) -> NBCContent.Type? {

        //
        // Content types available in data feed
        let NBCContentTypes: [String: NBCContent.Type] = [
            "web": NBCContentWeb.self,
            "hero": NBCContentHero.self,
            "video": NBCContentVideo.self,
            "section": NBCContentSection.self,
            "article": NBCContentArticle.self,
            "videos": NBCContentVideos.self,
            "promo": NBCContentPromo.self,
            "channels": NBCContentChannels.self,
            "slideshow": NBCContentSlideshow.self
        ]
        
        guard let type = json["type"].string?.lowercased() else {
            print("[json-parse] content type missing\n \(json)")
            return nil
        }
        
        guard let contentType = NBCContentTypes[type] else {
            print("[json-parse] unknown content type: '\(type)'")
            return nil
        }
        
        return contentType
    }
}


class NBCContentSection: NBCContent {
    let items: [NBCContent]
    
    required init?(json: JSON) {
        
        self.items = json["items"].arrayValue.compactMap {
            
            guard let contentType = NBCContent.type(json: $0) else {
                return nil
            }
            
            return contentType.init(json: $0)
        }
        
        super.init(json: json)
    }
    
}

class NBCContentVideos: NBCContent {
    
}

class NBCContentSlideshow: NBCContent {
    
}

class NBCContentHero: NBCContent {
    let item: NBCContent
    
    required init?(json: JSON) {
        
        guard let contentType = NBCContent.type(json: json) else {
            return nil
        }
        
        guard let item = contentType.init(json: json["item"]) else {
            print("[json-parse] item missing from hero\n \(json)")
            return nil
        }
        
        self.item = item
        
        super.init(json: json)
    }
}

class NBCContentVideo: NBCContent {
    
    var headline: String {
        get { return store["headline"].stringValue }
    }
    
    var published: Date? {
        get { return store["url"].string?.dateFromISO8601 }
    }
    
    var teaser: URL? {
        get { return store["teaser"].url }
    }
    
    var label: String {
        get { return store["label"].stringValue }
    }
    
    var summary: String {
        get { return store["summary"].stringValue }
    }
    
    var videoUrl: URL? {
        get { return store["videoUrl"].url }
    }
    
}

class NBCContentWeb: NBCContent {
    
}

class NBCContentArticle: NBCContent {
    var headline: String {
        get { return store["headline"].stringValue }
    }
    
    var published: Date? {
        get { return store["url"].string?.dateFromISO8601 }
    }
    
    var teaser: URL? {
        get { return store["teaser"].url }
    }
    
    var label: String {
        get { return store["label"].stringValue }
    }
    
    var summary: String {
        get { return store["summary"].stringValue }
    }

}

class NBCContentPromo: NBCContent {
    
}

class NBCContentChannels: NBCContent {
    
}
