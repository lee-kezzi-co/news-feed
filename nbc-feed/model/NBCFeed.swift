//
//  NBCFeed.swift
//  super-video
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import SwiftyJSON

class NBCFeed {
    private let store: JSON
    
    var items:[NBCContent] = []
    var tracking: NSObject?
    
    init(json: JSON) {
        self.store = json
        
        self.items = store["data"].arrayValue.compactMap { itemJson -> NBCContent? in
            guard let contentType = NBCContent.type(json: itemJson) else {
                return nil
            }
            
            guard let content = contentType.init(json: itemJson) else {
                return nil
            }
            
            return content
        }
        
    }

}
