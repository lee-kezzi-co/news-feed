//
//  JSONClient.swift
//  super-video
//
//  Created by Lee Irvine on 8/8/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import SwiftyJSON

struct ClientError: Error {
  let status: Int
  let message: String
}

struct ErrorMessage: Error {
    let message: String
}

enum HTTPVerb: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
}


class JSONClient: NSObject {

    private class func addParamsToUrl(_ url: URL, params:[String: Any]) -> URL {
        var queryUrl = url
        if var components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
            components.queryItems = params.map({ URLQueryItem(name: $0.key, value: "\($0.value)")})
          queryUrl = components.url!
        }

        return queryUrl
    }

    class func request(_ url: URL, method: HTTPVerb = .get, completion:@escaping (JSON?, Error?) ->()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)

        request.httpMethod = method.rawValue

        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            DispatchQueue.main.async {
                print("\(method) \(url)")

                UIApplication.shared.isNetworkActivityIndicatorVisible = false

                guard let response = urlResponse as? HTTPURLResponse else {
                    print("unexpected response type")
                    return
                }

                print("RESPONSE \(response.statusCode)")
                guard 200..<300 ~= response.statusCode else {
                    let message = data == nil ? "" : String(data: data!, encoding: String.Encoding.ascii )
                    completion(nil, ErrorMessage(message: message ?? ""))
                    return
                }

                guard let data = data else {
                    completion(nil, nil)
                    return
                }

                print(String(data: data, encoding: .utf8)!)

                do {
                    let json = try JSON(data: data)
                    completion(json, nil)

                } catch let error {
                    completion(nil, ClientError(status: 0, message:">>>>> Failed to parse response \(url) \(error.localizedDescription)"))
                }
            }
        }

        task.resume()

    }


}
