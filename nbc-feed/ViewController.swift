//
//  ViewController.swift
//  nbc-feed
//
//  Created by Lee Irvine on 2/18/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var activitySpinner: UIActivityIndicatorView!
    var feedTableVc: FeedTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // add loading spinner
        self.activitySpinner = UIActivityIndicatorView(frame: CGRect.zero)
        self.view.addSubview(self.activitySpinner)
        
        self.activitySpinner.startAnimating()
        self.activitySpinner.centerXToSuperview()
        self.activitySpinner.centerYToSuperview()
        
        // add content table
        self.feedTableVc = FeedTableViewController(nibName: nil, bundle: nil)
        self.view.addSubview(self.feedTableVc.tableView)
        self.feedTableVc.tableView.pinEdges(to: self.view)
        self.feedTableVc.tableView.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        let nbcUrl = URL(string: "https://s3.amazonaws.com/shrekendpoint/news.json")!
        
        JSONClient.request(nbcUrl) { json, error in
            guard error == nil else { return }
            guard let json = json else { return }
            
            self.feedTableVc.feed = NBCFeed(json: json)
            self.feedTableVc.tableView.isHidden = false
            
            self.activitySpinner.stopAnimating()
        }
    }

}

